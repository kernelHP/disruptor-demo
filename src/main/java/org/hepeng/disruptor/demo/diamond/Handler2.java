package org.hepeng.disruptor.demo.diamond;

import com.lmax.disruptor.EventHandler;
import lombok.extern.java.Log;

import java.util.Random;

/**
 * @author he peng
 */

@Log
public class Handler2 implements EventHandler<Order> {

    @Override
    public void onEvent(Order event, long sequence, boolean endOfBatch) throws Exception {
        Random random = new Random();
        if (random.nextBoolean()) {
            RuntimeException ex = new RuntimeException("Handler 2 Exception");
            event.setEx(ex);
        }
        log.info("Handler 2 , order - > " + event + " , hash = " + event.hashCode());

        Thread.sleep(1000);

    }
}
