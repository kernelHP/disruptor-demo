package org.hepeng.disruptor.demo.diamond;

import lombok.Data;

/**
 * @author he peng
 */

@Data
public class Order {

    String orderNo;
    Long timestamp;
    Throwable ex;

}
