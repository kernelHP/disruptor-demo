package org.hepeng.disruptor.demo.diamond;

import com.lmax.disruptor.EventHandler;
import lombok.extern.java.Log;

import java.util.concurrent.CountDownLatch;

/**
 * @author he peng
 */

@Log
public class LastHandler implements EventHandler<Order> {

    private CountDownLatch latch;

    public LastHandler(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void onEvent(Order event, long sequence, boolean endOfBatch) throws Exception {
        log.info("**************************** LastHandler , order - > " + event + " , hash = " + event.hashCode());
        Thread.sleep(1000);
        latch.countDown();
    }
}
