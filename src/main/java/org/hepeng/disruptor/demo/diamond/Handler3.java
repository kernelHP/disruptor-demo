package org.hepeng.disruptor.demo.diamond;

import com.lmax.disruptor.EventHandler;
import lombok.extern.java.Log;

/**
 * @author he peng
 */

@Log
public class Handler3 implements EventHandler<Order> {

    @Override
    public void onEvent(Order event, long sequence, boolean endOfBatch) throws Exception {
        log.info("Handler 3 , order - > " + event + " , hash = " + event.hashCode());
        Thread.sleep(1000);
    }
}
