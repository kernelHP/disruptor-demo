package org.hepeng.disruptor.demo.diamond;

import com.lmax.disruptor.EventTranslator;
import lombok.Getter;

/**
 * @author he peng
 * @date 2020/9/11
 */

public class SequenceAwareEventTranslator implements EventTranslator<Order> {

    @Getter
    public Long sequence;


    @Override
    public void translateTo(Order event, long sequence) {
        this.sequence = sequence;
        event.setOrderNo("Order-1");
        event.setTimestamp(System.currentTimeMillis());
    }
}
