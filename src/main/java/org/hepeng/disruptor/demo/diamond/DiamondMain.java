package org.hepeng.disruptor.demo.diamond;

import com.lmax.disruptor.BusySpinWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.EventTranslator;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.EventHandlerGroup;
import com.lmax.disruptor.dsl.ProducerType;
import lombok.Getter;
import lombok.extern.java.Log;

import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author he peng
 */

@Log
public class DiamondMain {

    public static void main(String[] args) throws Exception {

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        Disruptor<Order> disruptor = new Disruptor(new EventFactory<Order>() {
            @Override
            public Order newInstance() {
                return new Order();
            }
        } , 1024 , executorService , ProducerType.SINGLE , new BusySpinWaitStrategy());

        CountDownLatch countDownLatch = new CountDownLatch(1);

        EventHandlerGroup<Order> eventHandlerGroup = disruptor.handleEventsWith(
                new Handler1(), new Handler2(), new Handler3()).then(new LastHandler(countDownLatch));

        disruptor.start();


        SequenceAwareEventTranslator eventTranslator = new SequenceAwareEventTranslator();

        disruptor.publishEvent(eventTranslator);

        countDownLatch.await();

        Order order = disruptor.get(eventTranslator.getSequence());

        log.info("处理完成后的数据 -> " + order + " , hash = " + order.hashCode());

        disruptor.shutdown();
        executorService.shutdown();
    }
}
