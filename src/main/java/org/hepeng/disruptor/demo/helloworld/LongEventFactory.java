package org.hepeng.disruptor.demo.helloworld;

import com.lmax.disruptor.EventFactory;

/**
 * @author he peng
 */
public class LongEventFactory implements EventFactory<LongEvent> {

    public LongEvent newInstance() {
        return new LongEvent();
    }
}
