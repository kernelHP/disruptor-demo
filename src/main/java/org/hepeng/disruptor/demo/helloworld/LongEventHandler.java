package org.hepeng.disruptor.demo.helloworld;


import com.lmax.disruptor.EventHandler;
import lombok.extern.java.Log;

/**
 * @author he peng
 */

@Log
public class LongEventHandler implements EventHandler<LongEvent> {

    public void onEvent(LongEvent event, long sequence, boolean endOfBatch) throws Exception {
        log.info("Event: " + event);
    }
}
