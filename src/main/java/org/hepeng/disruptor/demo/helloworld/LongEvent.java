package org.hepeng.disruptor.demo.helloworld;

import lombok.Data;

/**
 * @author he peng
 */

@Data
public class LongEvent {

    private Long value;

}
